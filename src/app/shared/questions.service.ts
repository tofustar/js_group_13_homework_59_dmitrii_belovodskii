import { Question } from './question.model';

export class QuestionsService {
  index = 0;
  private questions: Question [] = [
    new Question('Сколько будет 2 + 2?', '4', 'На один больше, чем 3', ''),
    new Question('Сколько зубов у человека?', '32', '16 зубов на одном ряду', ''),
    new Question('Сколько ног у насекомго?', '6', '3 пары ног', '')
  ];

  getQuestions() {
    return this.questions.slice();
  }
}
