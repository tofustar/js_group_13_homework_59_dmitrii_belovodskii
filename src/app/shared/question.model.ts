export class Question {
  constructor(
    public textQuestion: string,
    public correctAnswer: string,
    public hint: string,
    public statusAnswer: string
  ) {}
}
