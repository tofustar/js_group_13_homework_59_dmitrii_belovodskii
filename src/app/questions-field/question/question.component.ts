import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { QuestionsService } from '../../shared/questions.service';
import { Question } from '../../shared/question.model';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  questions!: Question[];
  status = false;
  @Input() question = '';
  @Input() index = 0;
  @Input() hint = '';
  @ViewChild('answerInput') answerInput!: ElementRef;


  constructor(private questionService: QuestionsService) { }

  ngOnInit(){
    this.questions = this.questionService.getQuestions();
  }

  sendAnswer() {
    this.questionService.index = this.index;
    const answer = this.answerInput.nativeElement.value;
    if (answer === this.questions[this.index].correctAnswer) {
      this.questions[this.index].statusAnswer = 'Correct';
    } else if (answer === '') {
      this.questions[this.index].statusAnswer = 'You have not entered anything';
    }
    else {
      this.questions[this.index].statusAnswer = 'Wrong';
    }
  }
}
