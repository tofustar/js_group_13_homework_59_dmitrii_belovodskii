import { Component, OnInit } from '@angular/core';
import { Question } from '../shared/question.model';
import { QuestionsService } from '../shared/questions.service';

@Component({
  selector: 'app-questions-field',
  templateUrl: './questions-field.component.html',
  styleUrls: ['./questions-field.component.css']
})
export class QuestionsFieldComponent implements OnInit {
  questions!: Question[];

  constructor(private questionService: QuestionsService) { }

  ngOnInit(){
    this.questions = this.questionService.getQuestions();
  }

}
