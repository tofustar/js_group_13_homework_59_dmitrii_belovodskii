import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { QuestionsFieldComponent } from './questions-field/questions-field.component';
import { QuestionComponent } from './questions-field/question/question.component';
import { QuestionsService } from './shared/questions.service';
import { GetHideAnswerDirective } from './directives/get-hide-answer.directive';
import { GetIlluminationDirective } from './directives/get-ilumination.directive';

@NgModule({
  declarations: [
    AppComponent,
    QuestionsFieldComponent,
    QuestionComponent,
    GetHideAnswerDirective,
    GetIlluminationDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [QuestionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
