import { Directive, ElementRef, HostListener, Input, OnInit, Renderer2 } from '@angular/core';
import { QuestionsService } from '../shared/questions.service';
import { Question } from '../shared/question.model';

@Directive({
  selector: '[getIllumination]'
})

export  class GetIlluminationDirective implements OnInit {
  questions!: Question [];
  @Input() set getIllumination (answer: string) {
    if (answer) {
      this.answer = answer;
    }
  }

  answer = '';

  constructor(private el: ElementRef, private renderer: Renderer2, private questionService: QuestionsService) {}

  ngOnInit(){
    this.questions = this.questionService.getQuestions();
  }

  @HostListener('click') addIllumination() {
    if(this.answer === 'Correct') {
      this.renderer.addClass(this.el.nativeElement, 'bg-primary');
    } else if (this.answer === 'Wrong') {
      this.renderer.addClass(this.el.nativeElement, 'bg-danger');
    }
  }
}
