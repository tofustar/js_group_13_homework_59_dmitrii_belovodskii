import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[getHideAnswer]'
})

export class GetHideAnswerDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {}


  @HostListener('click') createElement() {
    this.renderer.createElement('div', 'Open');
  }

}
